﻿using System;
using System.Linq;

namespace task5
{
    class Program
    {
        private abstract class Convolution
        {
            public Convolution(System.Drawing.Bitmap bitmap, uint size)
            {
                input = bitmap.Clone() as System.Drawing.Bitmap;
                kernelSize = size;
                kernel = new double[2 * kernelSize + 1, 2 * kernelSize + 1];
            }


            public System.Drawing.Bitmap Apply()
            {
                System.Drawing.Imaging.BitmapData inputData
                    =
                    input.LockBits(
                            new
                            System.Drawing.Rectangle(
                                0, 0, input.Width, input.Height),
                            System.Drawing.Imaging.ImageLockMode.ReadOnly,
                            System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
                System.Drawing.Bitmap result
                    = new System.Drawing.Bitmap(
                            input.Width, input.Height, input.PixelFormat);
                System.Drawing.Imaging.BitmapData resultData
                    =
                    result.LockBits(
                            new
                            System.Drawing.Rectangle(
                                0, 0, input.Width, input.Height),
                            System.Drawing.Imaging.ImageLockMode.WriteOnly,
                            System.Drawing.Imaging.PixelFormat.Format32bppPArgb);
                unsafe
                {
                    const uint NumberOfComponents = 4;

                    byte *inputArray = (byte *)inputData.Scan0;
                    int resultPixel(uint y, uint x)
                    {
                        int pixel = 0;
                        byte *writeByte = (byte *)&pixel;
                        foreach (byte pixelByte
                                in
                                from component
                                in Enumerable.Range(0, (int)NumberOfComponents)
                                select
                                (byte)
                                Math.Round(
                                    (
                                     from j in Enumerable.Range(
                                         (int)-kernelSize,
                                         (int)(2 * kernelSize + 1))
                                         from i in Enumerable.Range(
                                         (int)-kernelSize,
                                         (int)(2 * kernelSize + 1))
                                         select
                                         kernel[kernelSize + j, kernelSize + i]
                                         *
                                         inputArray[
                                         ToValidSecondIndex((int)y - j)
                                         * inputData.Stride
                                         + ToValidFirstIndex((int)x - i)
                                         * NumberOfComponents
                                         + component]).Sum()))
                            *writeByte++ = pixelByte;
                        return pixel;
                    }


                    int stride = resultData.Stride / (int)NumberOfComponents;
                    int *writePointer = (int *)resultData.Scan0;
                    foreach (uint y in Enumerable.Range(0, input.Height))
                    {
                        foreach (uint x in Enumerable.Range(0, input.Width))
                            *writePointer++ = resultPixel(y, x);
                        writePointer += stride - input.Width;
                    }
                }
                input.UnlockBits(inputData);
                result.UnlockBits(resultData);
                return result;
            }


            public System.Drawing.Bitmap ApplySafe()
            {
                System.Drawing.Color resultPixel(uint y, uint x)
                {
                    return
                        System.Drawing.Color.FromArgb(
                                (int)
                                BitConverter.ToUInt32(
                                    (
                                     from componentName
                                     in new string[]{"B", "G", "R", "A"}
                                     select
                                     (byte)
                                     Math.Round(
                                         (from j
                                          in
                                          Enumerable.Range(
                                              (int)-kernelSize,
                                              (int)(2 * kernelSize + 1))
                                              from i
                                              in
                                              Enumerable.Range(
                                                  (int)-kernelSize,
                                                  (int)(2 * kernelSize + 1))
                                              select
                                              kernel[
                                              kernelSize + j, kernelSize + i]
                                              *
                                              (byte)
                                              typeof(System.Drawing.Color)
                                              .GetProperty(componentName)
                                              .GetValue(
                                                  input.GetPixel(
                                                      (int)
                                                      ToValidFirstIndex(
                                                          (int)x - i),
                                                      (int)
                                                      ToValidSecondIndex(
                                                          (int)y - j)))
                                              ).Sum())).ToArray()));
                }


                System.Drawing.Bitmap result
                    = new System.Drawing.Bitmap(
                            input.Width, input.Height, input.PixelFormat);
                foreach (uint y in Enumerable.Range(0, input.Height))
                    foreach (uint x in Enumerable.Range(0, input.Width))
                        result.SetPixel((int)x, (int)y, resultPixel(y, x));
                return result;
            }


            private System.Drawing.Bitmap input;

            private int closestInRange(int start, int end, int value)
            {
                return
                    value < start ? start : value > end ? end : value;
            }


            private uint ToValidFirstIndex(int index)
            {
                return (uint)closestInRange(0, input.Width - 1, index);
            }


            private uint ToValidSecondIndex(int index)
            {
                return (uint)closestInRange(0, input.Height - 1, index);
            }


            protected double[,] kernel;
            protected uint kernelSize;
        }

        private class GaussianBlur : Convolution
        {
            public GaussianBlur(System.Drawing.Bitmap bitmap, uint size)
                : base(bitmap, size)
            {
                double standardNormalDistributionProbabiltyDensityFunction(
                        uint argument)
                {
                    uint Square(uint value)
                    {
                        return value * value;
                    }


                    return
                        Math.Exp(-(double)Square(argument) / 2)
                        / Math.Sqrt(2 * Math.PI);
                }


                double[]
                    standardNormalDistributionProbabiltyDensityFunctionArray
                    = (
                            from i
                            in Enumerable.Range(0, (int)(kernelSize + 1))
                            select
                            standardNormalDistributionProbabiltyDensityFunction(
                                (uint)i)).ToArray();

                foreach (int j in Enumerable.Range(0, (int)(kernelSize + 1)))
                    foreach (
                            int i in Enumerable.Range(0, (int)(kernelSize + 1)))
                        kernel[j, i]
                        =
                        kernel[j, 2 * kernelSize - i]
                        =
                        kernel[2 * kernelSize - j, i]
                        =
                        kernel[2 * kernelSize - j, 2 * kernelSize - i]
                        =
                        standardNormalDistributionProbabiltyDensityFunctionArray
                        [
                        kernelSize - i]
                        *
                        standardNormalDistributionProbabiltyDensityFunctionArray
                        [
                        kernelSize - j];
            }
        }

        private enum ExitCode {OK, Error};

        private static int Main(string[] args)
        {
            const string ProgramName = "task5";

            try
            {
                const uint KernelSize = 16;
                const string
                    InputFile = "input",
                    SafeOutputFile = "output_safe",
                    UnsafeOutputFile = "output_unsafe";

                using(
                        System.Drawing.Bitmap input
                        = new System.Drawing.Bitmap(InputFile))
                {
                    GaussianBlur blurHandle
                        = new GaussianBlur(input, KernelSize);
                    System.Diagnostics.Stopwatch stopwatch
                        = new System.Diagnostics.Stopwatch();
                    stopwatch.Start();
                    using (
                            System.Drawing.Bitmap unsafeResult
                            = blurHandle.Apply())
                    {
                        stopwatch.Stop();
                        unsafeResult.Save(UnsafeOutputFile);
                    }
                    System.Console.WriteLine(
                            "Unsafe ticks: " + stopwatch.ElapsedTicks);
                    stopwatch.Restart();
                    using (
                            System.Drawing.Bitmap safeResult
                            = blurHandle.ApplySafe())
                    {
                        stopwatch.Stop();
                        safeResult.Save(SafeOutputFile);
                    }
                    System.Console.WriteLine(
                            "Safe ticks:   " + stopwatch.ElapsedTicks);
                }
            }
            catch (System.IO.FileNotFoundException fileNotFoundException)
            {
                Console.Error.WriteLine(
                        ProgramName
                        + ": File '"
                        + fileNotFoundException.Message
                        + "' not found");
                return (int)ExitCode.Error;
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine(ProgramName + ": " + exception.Message);
                return (int)ExitCode.Error;
            }
            return (int)ExitCode.OK;
        }
    }
}
